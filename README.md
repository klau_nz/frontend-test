# Front-end Developer Coding Test

Before we begin, be sure to read all of this document carefully and follow the guidelines within.

## Context

The test comes with multiple branches, each branch will contain an assessment to test your front-end knowledge.

1. Quiz
2. HTML
3. Cascading Style Sheets (CSS)
4. JavaScript
5. jQuery
6. React.js

